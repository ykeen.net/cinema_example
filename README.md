# Cinema Example für KinoBaden (stark vereinfacht)

## Features

Barebone Template für Projekt KinoBaden mit nur 4 Model-Klassen, 4 DB-Tabellen, keiner Userverwaltung und keinem UserLogin.

- Einfache Auflistung von KalenderElementen mit Kinofilmen aus der Datenbank
- Hinzufügen eines Elements auf den Einkaufskorb
- Anzeigen des Einkaufskorbes

## Prerequirements

- Anlegen der Datenbank auf Localhost (SQL Server Express 2017, Gemischter Anmeldemodus mit User SA)
- SQL-Files zum Anlegen unter /Database (im create tables sql die fehlermeldungen wegen drop database ignorieren)
- In den Tables movies, calelements und user Beispieldaten anlegen (zB 3 Filme, 6 Termine für die Filme, 1 User)
- Im Visual Studio die Projektmappe neu erstellen (bereinigen, erstellen)

## Versionen

- Verwendet wurde das Visual Studio 2017 Community (2019 sollte also auch funktionieren)
- und Microsoft SQL Server Express 2017